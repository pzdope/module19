#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void Voice(){}
};

class Dog :public Animal
{
	void Voice() override
	{
		cout << "Dog say: Woof!" << endl;
	}
};

class Cat :public Animal
{
public:
	void Voice() override
	{
		cout << "Cat say: Meow!" << endl;
	}
};

class Cow :public Animal
{
	void Voice() override
	{
		cout << "Cow say: Moo!" << endl;
	}
};

int main()
{
	Dog MyDog;
	Cat MyCat;
	Cow MyCow;
	Animal* array[3] = { &MyDog, &MyCat, &MyCow };
	
	for (int i = 0; i < 3; ++i)
	{
		array[i]->Voice();
	}
}
